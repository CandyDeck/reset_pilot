The excel file called **”Raw data”** contains the unedited values from the sources. This file include the proximate and ultimate analysis, as well as the heating values, amount of halides and trace elements, and the ash components. 

The file called **”Data with calculated values”** contains the raw data, as well as
diverse calculated values. This file does not include the ash components and the amount trace elements.

